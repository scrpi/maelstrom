import random
import sys

from twisted.internet import reactor
from twisted.internet.defer import succeed, fail
from twisted.internet.task import deferLater
from twisted.web.client import getPage
#from zope.interface import implements

#from bravo.config import configuration
#from bravo.ibravo import IAuthenticator
#from bravo.packets import make_packet

from mael.packets import make_packet
  
server = "http://www.minecraft.net/game/checkserver.jsp?user=%s&serverId=%s"

class MaelAuthent(object):

    #implements(IAuthenticator)

    def __init__(self):

        self.challenges = {}

    def handshake(self, protocol, container):
        """
        Handle a handshake with an online challenge.
        """

        challenge = "%x" % random.randint(0, sys.maxint)
        self.challenges[protocol] = challenge

        packet = make_packet("handshake", username=challenge)

        d = deferLater(reactor, 0, protocol.challenged)
        d.addCallback(lambda none: protocol.transport.write(packet))

        return True

    def login(self, protocol, container):
        if protocol not in self.challenges:
            protocol.error("Didn't see your handshake.")
            return

        protocol.username = container.username
        challenge = self.challenges.pop(protocol)
        protocol.challenge = challenge
        url = server % (container.username, challenge)

        d = getPage(url.encode("utf8"))
        d.addCallback(self.success, protocol, container)
        d.addErrback(self.error, protocol)

        return d

    def success(self, response, protocol, container):
    
        if response != "YES":
            protocol.error("Authentication server didn't like you.")
            return

        players = min(protocol.factory.limitConnections, 60)

        packet = make_packet("login", protocol=191919, username="",
            seed=protocol.factory.fakeseed, mode="creative",
            dimension="earth", unknown=1, height=128,
            players=players)

        protocol.transport.write(packet)
        
        protocol.authenticated        
        
    def error(self, description, protocol):

        protocol.error("Couldn't authenticate: %s" % description)
        
    name = "MaelAuthent"
