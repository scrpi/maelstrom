from mael.packets import make_packet

class MaelCommands(object):

    def __init__(self):
        
        self.commands = {
            "portal": self.portal,
            "p1": self.p1,
            "p2": self.p2,
        }

    def portal(self, protocol, params):
        """
        Portal command - send the player to another minecraft server.
        """
        
        #args[0] should be a server address or address:port
        
        if len(params) == 2:
            if params[1]:
                port = params[1]
            else:
                port = "25565"
            
            port = int(port)
            host = params[0]
            
            packet = make_packet("effect", eid=191919, effect="confusion", 
                amount=0, duration=0)
            protocol.transport.write(packet)
            
            protocol.doPortal(host, port)
            
            #return "Portaling you..."
                        
        else:
            return "Usage: /portal [server] ([port])"

    def p1(self, protocol, params):
        self.portal(protocol, ["localhost", "25501"])
        
    def p2(self, protocol, params):
        self.portal(protocol, ["localhost", "25502"])        
