from collections import defaultdict
from itertools import chain, product
from urllib import urlencode
from urlparse import urlunparse

from twisted.internet import reactor
from twisted.internet.interfaces import IPushProducer
from twisted.internet.protocol import ServerFactory
from twisted.internet.task import LoopingCall
from twisted.python import log
from zope.interface import implements

from mael.protocol import MaelProtocol, KickedProtocol
from mael.authent import MaelAuthent
from mael.utilities.chat import chat_name, sanitize_chat
from mael.packets import make_packet

(STATE_UNAUTHENTICATED, STATE_CHALLENGED, STATE_AUTHENTICATED,
    STATE_LOCATED) = range(4)

circle = [(i, j)
    for i, j in product(xrange(-5, 5), xrange(-5, 5))
    if i**2 + j**2 <= 25
]

class MaelFactory(ServerFactory):
    """
    A ``Factory`` that creates ``MaelProtocol`` objects when connected to.
    """

    implements(IPushProducer)

    protocol = MaelProtocol

    timestamp = None
    time = 0
    day = 0
    eid = 1

    mode = 1

    interfaces = []

    def __init__(self, service):
        """
        """
        self.service = service
        self.protocols        = dict()
        self.connectedIPs     = defaultdict(int)
        self.limitConnections = 999999
        self.fakeseed = 1

    def startFactory(self):
        log.msg("Initializing factory...")

        authenticator = MaelAuthent

        log.msg("Using authenticator %s" % authenticator.name)
        self.handshake_hook = authenticator.handshake
        self.login_hook = authenticator.login

        log.msg("Factory successfully initialized!")

    def stopFactory(self):
        """
        Called before factory stops listening on ports. Used to perform
        shutdown tasks.
        """

    def buildProtocol(self, addr):
        """
        Create a protocol.
        """

        # Need to allow poll packets regardless of connection limit.
        #if (self.limitConnections
        #    and len(self.protocols) >= self.limitConnections):
        #    log.msg("Reached maximum players, turning %s away." % addr.host)
        #    p = KickedProtocol("The player limit has already been reached."
        #                       " Please try again later.")
        #    p.factory = self
        #    return p

        log.msg("Starting connection for %s" % addr)
        
        p = self.protocol()
        p.host = addr.host
        p.factory = self

        return p

    def chat(self, message):
        """
        Relay chat messages.

        Chat messages are sent to all connected clients, as well as to anybody
        consuming this factory.
        """

        # Prepare the message for chat packeting.
        for user in self.protocols:
            message = message.replace(user, chat_name(user))
        message = sanitize_chat(message)

        log.msg("Chat: %s" % message.encode("utf8"))

        packet = make_packet("chat", message=message)
        self.broadcast(packet)
        
    def broadcast(self, packet):
        """
        Broadcast a packet to all connected players.
        """

        for player in self.protocols.itervalues():
            player.transport.write(packet)

    def broadcast_for_others(self, packet, protocol):
        """
        Broadcast a packet to all players except the originating player.

        Useful for certain packets like player entity spawns which should
        never be reflexive.
        """

        for player in self.protocols.itervalues():
            if player is not protocol:
                player.transport.write(packet)
