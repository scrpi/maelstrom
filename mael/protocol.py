# vim: set fileencoding=utf8 :

from itertools import product, chain
from time import time
from urlparse import urlunparse
from math import pi

from twisted.internet import reactor
from twisted.internet.defer import (DeferredList, inlineCallbacks,
    maybeDeferred, succeed)
from twisted.internet.protocol import Protocol
from twisted.internet.task import cooperate, deferLater, LoopingCall
from twisted.internet.task import TaskDone, TaskFailed
from twisted.protocols.policies import TimeoutMixin
from twisted.python import log
from twisted.web.client import getPage

from mael.packets import parse_packets, make_packet, make_error_packet, make_packet_from_container
from mael.utilities.temporal import timestamp_from_clock
from mael.realm.factory import RealmFactory
from mael.authent import MaelAuthent
from mael.commands import MaelCommands

(STATE_UNAUTHENTICATED, STATE_CHALLENGED, STATE_AUTHENTICATED) = range(3)

(REALM_DISCONNECTED, REALM_CONNECTED, REALM_AUTHENTICATED) = range(3)

SUPPORTED_PROTOCOL = 17

circle = [(i, j)
    for i, j in product(xrange(-10, 10), xrange(-10, 10))
    if i**2 + j**2 <= 100
]
"""
A list of points in a filled circle of radius 10.
"""

class BetaServerProtocol(object, Protocol, TimeoutMixin):
    """
    The Minecraft Alpha/Beta server protocol.

    This class is mostly designed to be a skeleton for featureful clients. It
    tries hard to not step on the toes of potential subclasses.
    """

    excess = ""
    packet = None

    state = STATE_UNAUTHENTICATED

    buf = ""
    parser = None
    handler = None

    player = None
    username = None
    motd = "Maelstrom Minecraft MMO Proxy"

    _health = 20
    _latency = 0
    
    connectedRealm      = set()
    connectedRealmState = REALM_DISCONNECTED

    def __init__(self):

        self.handlers = {
            0: self.ping,
            1: self.login,
            2: self.handshake,
            3: self.chat,
            #7: self.use,
            #9: self.respawn,
            #10: self.grounded,
            #11: self.position,
            #12: self.orientation,
            #13: self.location_packet,
            #14: self.digging,
            #15: self.build,
            #16: self.equip,
            #18: self.animate,
            #19: self.action,
            #21: self.pickup,
            #101: self.wclose,
            #102: self.waction,
            #106: self.wacknowledge,
            #107: self.creative_inventory,
            #130: self.sign,
            254: self.poll,
            255: self.quit,
        }

        self._ping_loop = LoopingCall(self.update_ping)

        self.setTimeout(30)

    # Low-level packet handlers
    # Try not to hook these if possible, since they offer no convenient
    # abstractions or protections.

    def ping(self, container):
        """
        Hook for ping packets.

        By default, this hook will examine the timestamps on incoming pings,
        and use them to estimate the current latency of the connected client.
        """

        now = timestamp_from_clock(reactor)
        then = container.pid

        self.latency = now - then

    def login(self, container):
        """
        Hook for login packets.

        Override this to customize how logins are handled. By default, this
        method will only confirm that the negotiated wire protocol is the
        correct version, and then it will run the ``authenticated()``
        callback.
        """

        if container.protocol < SUPPORTED_PROTOCOL:
            # Kick old clients.
            self.error("This server doesn't support your ancient client.")
        elif container.protocol > SUPPORTED_PROTOCOL:
            # Kick new clients.
            self.error("This server doesn't support your newfangled client.")
        else:
            reactor.callLater(0, self.authenticated)

    def handshake(self, container):
        """
        Hook for handshake packets.
        """

    def chat(self, container):
        """
        Hook for chat packets.
        """

    def use(self, container):
        """
        Hook for use packets.
        """

    def respawn(self, container):
        """
        Hook for respawn packets.
        """

    def grounded(self, container):
        """
        Hook for grounded packets.
        """

        self.location.grounded = bool(container.grounded)

    def position(self, container):
        """
        Hook for position packets.
        """

    def orientation(self, container):
        """
        Hook for orientation packets.
        """

    def location_packet(self, container):
        """
        Hook for location packets.
        """

    def digging(self, container):
        """
        Hook for digging packets.
        """

    def build(self, container):
        """
        Hook for build packets.
        """

    def equip(self, container):
        """
        Hook for equip packets.
        """

    def pickup(self, container):
        """
        Hook for pickup packets.
        """

    def animate(self, container):
        """
        Hook for animate packets.
        """

    def action(self, container):
        """
        Hook for action packets.
        """

    def wclose(self, container):
        """
        Hook for wclose packets.
        """

    def waction(self, container):
        """
        Hook for waction packets.
        """

    def wacknowledge(self, container):
        """
        Hook for wacknowledge packets.
        """

    def creative_inventory(self, container):
        """
        Hook for creative inventory action packets.
        """

    def sign(self, container):
        """
        Hook for sign packets.
        """

    def poll(self, container):
        """
        Hook for poll packets.

        By default, queries the parent factory for some data, and replays it
        in a specific format to the requester. The connection is then closed
        at both ends. This functionality is used by Beta 1.8 clients to poll
        servers for status.
        """

        players = len(self.factory.protocols)
        max_players = self.factory.limitConnections or 1000000

        response = u"%s§%d§%d" % (self.motd, players, max_players)
        log.msg("Sending poll response to client at %s" % self.host)
        log.msg("Protocols: %s" % self.factory.protocols)
        self.error(response)

    def quit(self, container):
        """
        Hook for quit packets.

        By default, merely logs the quit message and drops the connection.

        Even if the connection is not dropped, it will be lost anyway since
        the client will close the connection. It's better to explicitly let it
        go here than to have zombie protocols.
        """

        log.msg("Client is quitting: %s" % container.message)
        self.transport.loseConnection()

    # Twisted-level data handlers and methods
    # Please don't override these needlessly, as they are pretty solid and
    # shouldn't need to be touched.

    def dataReceived(self, data):

        self.buf += data

        packets, self.buf = parse_packets(self.buf)

        if packets:
            self.resetTimeout()

        for header, payload in packets:
            if header in self.handlers:
                self.handlers[header](payload)
            else:
                if self.connectedRealm and self.connectedRealmState == REALM_AUTHENTICATED:
                    packet = make_packet_from_container(header, payload)
                    self.connectedRealm.transport.write(packet)

    def connectionLost(self, reason):
        if self._ping_loop.running:
            self._ping_loop.stop()

    def timeoutConnection(self):
        self.error("Connection timed out")

    # State-change callbacks
    # Feel free to override these, but call them at some point.

    def challenged(self):
        """
        Called when the client has started authentication with the server.
        """

        self.state = STATE_CHALLENGED

    def authenticated(self):
        """
        Called when the client has successfully authenticated with the server.
        """

        self.state = STATE_AUTHENTICATED

        self._ping_loop.start(30)

    # Event callbacks
    # These are meant to be overriden.

    def orientation_changed(self):
        """
        Called when the client moves.

        This callback is only for orientation, not position.
        """

        pass

    def position_changed(self):
        """
        Called when the client moves.

        This callback is only for position, not orientation.
        """

        pass

    # Convenience methods for consolidating code and expressing intent. I
    # hear that these are occasionally useful.

    def write_packet(self, header, **payload):
        """
        Send a packet to the client.
        """

        self.transport.write(make_packet(header, **payload))

    def update_ping(self):
        """
        Send a keepalive to the client.
        """

        timestamp = timestamp_from_clock(reactor)
        self.write_packet("ping", pid=timestamp)

    def error(self, message):
        """
        Error out.

        This method sends ``message`` to the client as a descriptive error
        message, then closes the connection.
        """

        self.transport.write(make_error_packet(message))
        self.transport.loseConnection()

class KickedProtocol(BetaServerProtocol):
    """
    A very simple Beta protocol that helps enforce IP bans, Max Connections,
    and Max Connections Per IP.

    This protocol disconnects people as soon as they connect, with a helpful
    message.
    """

    def __init__(self, reason=None):
        if reason:
            self.reason = reason
        else:
            self.reason = (
                "This server doesn't like you very much."
                " I don't like you very much either.")

    def connectionMade(self):
        self.error("%s" % self.reason)

class MaelProtocol(BetaServerProtocol):
    """
    A ``BetaServerProtocol`` suitable for serving MC worlds to clients.

    This protocol really does need to be hooked up with a ``MaelFactory`` or
    something very much like it.
    """

    time_loop = None
    authenticator = MaelAuthent()
    chatCommands = MaelCommands()
            
    def __init__(self):
        BetaServerProtocol.__init__(self)
        log.msg("Starting MaelProtocol")

    #@inlineCallbacks
    def authenticated(self):
        BetaServerProtocol.authenticated(self)

        self.factory.protocols[self.username] = self
        
        self.start_proxy("localhost:25501")
        
    def start_proxy(self, addr):
        log.msg("Starting proxy %s ..." % addr)
        address, port = addr.split(":")
        self.add_realm(address, int(port))

    def add_realm(self, address, port):
        """
        Add a new realm to this client.
        """

        from twisted.internet.endpoints import TCP4ClientEndpoint

        log.msg("Adding realm %s:%d" % (address, port))

        endpoint = TCP4ClientEndpoint(reactor, address, port, 5)
    
        self.realm_factory = self.factory.service.realm_factory
        self.realm_factory.player = self
        d = endpoint.connect(self.realm_factory)
        d.addCallback(self.realm_connected)
        d.addErrback(self.realm_connect_error)

    def realm_connected(self, protocol):
        log.msg("Connected new realm!")
        self.connectedRealmState = REALM_CONNECTED
        self.connectedRealm = protocol

    def realm_connect_error(self, reason):
        log.err("Couldn't connect realm!")
        log.err(reason)    

    def login(self, container):
        """
        Handle a login packet.

        This method wraps a login hook which is permitted to do just about
        anything, as long as it's asynchronous. The hook returns a
        ``Deferred``, which is chained to authenticate the user or disconnect
        them depending on the results of the authentication.
        """
        
        username = container.username

        if username in self.factory.protocols:
            self.error("Your username is already taken.")
            return

        if container.protocol < SUPPORTED_PROTOCOL:
            # Kick old clients.
            self.error("This server doesn't support your ancient client.")
            return
        elif container.protocol > SUPPORTED_PROTOCOL:
            # Kick new clients.
            self.error("This server doesn't support your newfangled client.")
            return

        log.msg("Authenticating client, protocol version %d" %
            container.protocol)

        d = self.authenticator.login(self, container)
        d.addErrback(lambda *args, **kwargs: self.transport.loseConnection())
        d.addCallback(lambda *args, **kwargs: self.authenticated())
        
    def handshake(self, container):
        """
        """
        self.authenticator.handshake(self, container)
        
    def connectionLost(self, reason):

        log.msg("Ending connection for %s" % self.host)
        
        if self.connectedRealm and self.connectedRealmState > REALM_DISCONNECTED:
            self.connectedRealm.doQuit()

        if self.username in self.factory.protocols:
            del self.factory.protocols[self.username]


    def quit(self, container):
    
        if self.connectedRealm and self.connectedRealmState > REALM_DISCONNECTED:
            self.connectedRealm.doQuit()
            
        log.msg("Client is quitting: %s" % container.message)
        self.transport.loseConnection()     
        
    def chat(self, container):
        if container.message.startswith("/"):

            params = container.message[1:].split(" ")
            command = params.pop(0).lower()

            if command and command in self.chatCommands.commands:
                def cb(msg):
                    #for line in iterable:
                    self.write_packet("chat", message=msg)
                def eb(error):
                    self.write_packet("chat", message="Error: %s" %
                        error.getErrorMessage())
                d = maybeDeferred(self.chatCommands.commands[command],
                                  self, params)
                d.addCallback(cb)
                d.addErrback(eb)
            else:
                self.write_packet("chat",
                    message="Unknown command: %s" % command)
        else:
            # Send the message up to the factory to be chatified.
            message = "<%s> %s" % (self.username, container.message)
            self.factory.chat(message)

    def doPortal(self, host, port):
        self.connectedRealmState = REALM_DISCONNECTED
        self.connectedRealm.doQuit()
        self.add_realm(host, port)
        




