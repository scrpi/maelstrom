from twisted.internet.protocol import ClientFactory
from twisted.python import log

from mael.realm.protocol import RealmProtocol

class RealmFactory(ClientFactory):
    """
    A ``Factory`` that serves as a Hydra client.
    """

    protocol = RealmProtocol

    def __init__(self, service):
        self.protocols = set()
        self.service = service
        
        log.msg("Hydra Client started")
        
    def startedConnecting(self, connector):
        print 'Started to connect.'

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection.  Reason:', reason

    def clientConnectionFailed(self, connector, reason):
        print 'Connection failed. Reason:', reason 

