from itertools import product, chain
from time import time
from urlparse import urlunparse
from math import pi

from twisted.internet import reactor
from twisted.internet.defer import (DeferredList, inlineCallbacks,
    maybeDeferred, succeed)
from twisted.internet.protocol import Protocol
from twisted.internet.task import cooperate, deferLater, LoopingCall
from twisted.internet.task import TaskDone, TaskFailed
from twisted.protocols.policies import TimeoutMixin
from twisted.python import log
from twisted.web.client import getPage

from mael.packets import parse_packets, make_packet, make_error_packet, make_packet_from_container

(REALM_DISCONNECTED, REALM_CONNECTED, REALM_AUTHENTICATED) = range(3)

class RealmProtocol(Protocol):

    buf = ""

    def __init__(self):
        #self.chunks = dict()
        #self.windows = []
        #self.wid = 1

        #self.location = Location()

        self.handlers = {
            0: self.ping,
            1: self.login,
            2: self.handshake,
            41: self.effect,
        }
        
    def connectionMade(self):
        self.doHandshake()
        #self.doLogin()

    def connectionLost(self, reason):
        self.factory.player.connectedRealmState = REALM_DISCONNECTED

    def dataReceived(self, data):
        self.buf += data

        packets, self.buf = parse_packets(self.buf)

        for header, payload in packets:
            if header in self.handlers:
                self.handlers[header](payload)
            else:
                #log.err("Didn't handle parseable packet %d!" % header)
                #log.err(payload)             
                packet = make_packet_from_container(header, payload)
                #log.msg("Proxying packet to player %d %s" %(header, packet))
                self.factory.player.transport.write(packet)

    def doLogin(self):
        """
        Sends a login packet to the Hydra Server
        """
        packet = make_packet("login", protocol=17, seed=0, mode="creative",
            unknown=0, height=0, players=0, dimension="earth",
            username=self.factory.player.username)
        log.msg("Sending Login Packet: %s" % packet)
        self.transport.write(packet)
        
    def login(self, payload):
        log.msg("Received login payload from realm: %s" % len(payload))
        self.factory.player.connectedRealmState = REALM_AUTHENTICATED

    def doHandshake(self):
        """
        Sends a Handshake packet to the Hydra Server
        """

        packet = make_packet("handshake",username=self.factory.player.username)
            
        log.msg("Sending Handshake Packet: %s" % packet)
        self.transport.write(packet)
        
    def handshake(self, payload):
        log.msg("Received handshake payload from realm: %s" % len(payload))
        self.doLogin()

    def ping(self, payload):
        packet = make_packet("ping", pid=payload.pid)
        self.transport.write(packet)
        
    def doQuit(self):
    
        packet = make_packet("error", message="quitting")
        log.msg("Sending Quit Packet to Realm: %s" % packet)
        self.transport.write(packet)
        self.transport.loseConnection()
        
    def effect(self, payload):
        log.msg("Effect")
        log.msg(payload.eid)
        log.msg(payload.effect)
        log.msg(payload.amount)
        log.msg(payload.duration)
        
