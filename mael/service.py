from twisted.application.service import Application, MultiService
from twisted.application.strports import service as serviceForEndpoint
from twisted.application import internet

from mael.factory import MaelFactory
from mael.realm.factory import RealmFactory

class MaelService(MultiService):

    def __init__(self):
        MultiService.__init__(self)
                
        self.client_factory = MaelFactory(self)
        interface = "tcp:25565"
        server = serviceForEndpoint(interface, self.client_factory)
        server.setName("Maelstrom")
        
        self.realm_factory = RealmFactory(self)
        
        
        #server = internet.TCPServer(interface, factory)
        
        self.addService(server)
        
    def addService(self, service):
        MultiService.addService(self, service)

    def removeService(self, service):
        MultiService.removeService(self, service)

service = MaelService()

application = Application("Maelstrom")
service.setServiceParent(application)
