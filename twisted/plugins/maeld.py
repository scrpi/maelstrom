from zope.interface import implements

from twisted.application.service import IServiceMaker
from twisted.plugin import IPlugin
from twisted.python.usage import Options

class MaelOptions(Options):
    pass

class MaelServiceMaker(object):

    implements(IPlugin, IServiceMaker)

    tapname = "mael"
    description = "Minecraft MMO Proxy daemon"
    options = MaelOptions

    def makeService(self, options):
        from mael.service import service
        return service

msm = MaelServiceMaker()
